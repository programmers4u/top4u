(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 40)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 44
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

})(jQuery); // End of use strict

function loadScript(url, callback)
{
    // Adding the script tag to the head as suggested before
    var head = document.head;
    var script = document.createElement('script');
    script.type = 'text/javascript';
    script.src = url;

    // Then bind the event to the callback function.
    // There are several events for cross browser compatibility.
    script.onreadystatechange = callback;
    script.onload = callback;

    // Fire the loading
    head.appendChild(script);
}

/* Global site tag (gtag.js) - Google Analytics */

loadScript("https://www.googletagmanager.com/gtag/js?id=UA-151371279-1",()=>{
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-151371279-1');
});

/** 
* Animate 
*/
var addAnim = function(ele){
  $(ele).removeClass('animated bounceInLeft delay-1s');
  setTimeout(function(ele){
    $(ele).addClass('animated bounceInLeft delay-1s');
  },500,ele);
}

function animateCSS(element, animationName, callback) {
  const node = element;//document.querySelector(element)
  node.classList.add('animated','fast', animationName)

  function handleAnimationEnd() {
      node.classList.remove('animated', animationName)
      node.removeEventListener('animationend', handleAnimationEnd)

      if (typeof callback === 'function') callback()
  }

  node.addEventListener('animationend', handleAnimationEnd)
}
// scroll
var $window = $(window);
function isScrolledIntoView($elem, $window) {
    var docViewTop = $window.scrollTop();
    var docViewBottom = docViewTop + $window.height();
    
    if(typeof $($elem) == 'undefined' ) return;
    
    var elemTop = $elem.offset().top-250;
    var elemBottom = elemTop + $elem.height();

    return ((elemTop <= docViewTop));//(elemBottom >= docViewBottom) && 
}

/**
 * Scroll event
 */
var scrollStopEvent = {
  'services': false,  
  'technology': false,
  'wow': false,
  'cta': false,
  'products': false,
  'contact': false,
}

/**
*	Start strony
*/
document.addEventListener('readystatechange', event => {
	if (event.target.readyState === "interactive") {
    }

    if (event.target.readyState === "complete") {
		setTimeout( function(){
	        InitSite();
		},400);
    }
});

var InitSite = function(){
  $('.loader').toggle();
  
  if( typeof $('#slide0').offset()  != "undefined") {
    //$('#slide1').addClass('shadow');
    for(let sI=2;sI<7;sI++){
      //$('.wow #slide'+sI).css('visibility','hidden');
      $('.wow #slide'+sI).addClass('filtrOn');
    }
  }

  if( typeof $('.services p').offset()  != "undefined") {
    document.querySelectorAll('.services p').forEach(o=>{o.classList.add('anim-hide');}); 
    document.querySelectorAll('.services h5').forEach(o=>{o.classList.add('anim-hide');}); 
    document.querySelectorAll('.services img').forEach(o=>{o.classList.add('anim-hide');}); 
  }  
  //

  if( typeof $('#technology').offset()  != "undefined") {
    document.querySelectorAll('#technology h2').forEach(o=>{o.classList.add('anim-hide');}); 
    document.querySelectorAll('#technology img').forEach(o=>{o.classList.add('anim-hide');}); 
  }

  if( typeof $('.cta').offset()  != "undefined") {
    document.querySelectorAll('.cta h2').forEach(o=>{o.classList.add('anim-hide');}); 
    document.querySelectorAll('.cta .col-lg-4').forEach(o=>{o.classList.add('anim-hide');}); 
    document.querySelectorAll('.cta .col-lg-8').forEach(o=>{o.classList.add('anim-hide');}); 
  }

  if( typeof $('.wow').offset()  != "undefined") {
    document.querySelectorAll('.wow').forEach(o=>{o.classList.add('anim-hide');}); 
  }

  if( typeof $('.products').offset()  != "undefined") {
    document.querySelectorAll('.products').forEach(o=>{o.classList.add('anim-hide');}); 
  }

  if( typeof $('.contact').offset()  != "undefined") {
    document.querySelectorAll('.contact').forEach(o=>{o.classList.add('anim-hide');}); 
  }

}

var lastWidth = document.documentElement.clientWidth;  


$(document).on("scroll", function () {

  if( typeof $('.services').offset()  != "undefined") {
  
    if (isScrolledIntoView($(".services"), $window) && scrollStopEvent.services == false) {
      scrollStopEvent.services = true;
      let ele = $('#services .col-lg-4');

      //document.querySelectorAll('.services p').forEach(o=>{o.classList.add('anim-hide')}); 
      //document.querySelectorAll('.services img').forEach(o=>{o.classList.add('anim-hide')}); 
      //document.querySelectorAll('.services h5').forEach(o=>{o.classList.add('anim-hide')}); 
      ele.each(function(index,value){
        value.querySelector('img').classList.remove('animated','delay-1s','bounceIn');
        value.querySelector('h5').classList.remove('animated','delay-1s','fadeIn');
        value.querySelector('p').classList.remove('animated', 'delay-1s','fadeIn');
        //value.classList.add('anim-hide');
      });

      animateCSS(ele[0].querySelector('img'),'bounceIn', function(){
        ele[0].querySelector('img').classList.remove('anim-hide'); 
        ele[0].querySelector('h5').classList.remove('anim-hide');
        ele[0].classList.remove('anim-hide');
        ele[0].querySelector('h5').classList.add('fadeIn');
        animateCSS(ele[1].querySelector('img'),'bounceIn', function(){
            ele[1].querySelector('img').classList.remove('anim-hide'); 
            ele[1].querySelector('h5').classList.remove('anim-hide');
            ele[1].classList.remove('anim-hide');
          ele[1].querySelector('h5').classList.add('fadeIn');
          animateCSS(ele[2].querySelector('img'),'bounceIn', function(){
            ele[2].querySelector('img').classList.remove('anim-hide'); 
            ele[2].querySelector('h5').classList.remove('anim-hide');
            ele[2].classList.remove('anim-hide');
            ele[2].querySelector('h5').classList.add('fadeIn');
            document.querySelectorAll('.services p').forEach(o=>{o.classList.remove('anim-hide');animateCSS(o,'bounceIn')}); 
          });
        });
      });
    }
  }

  if( typeof $('.contact').offset()  != "undefined") {

    if (isScrolledIntoView($('.contact'), $window) && scrollStopEvent.contact == false) {
      scrollStopEvent.contact = true;
      animateCSS($('.contact')[0],'fadeIn',function(){
        document.querySelectorAll('.contact').forEach(o=>{o.classList.remove('anim-hide');}); 
      });

    }
  }    

  if( typeof $('.technology').offset()  != "undefined") {

    if (isScrolledIntoView($(".technology"), $window) && scrollStopEvent.technology == false) {
      scrollStopEvent.technology = true;      

      document.querySelectorAll('#technology h2').forEach(o=>{o.classList.remove('anim-hide');}); 
      document.querySelectorAll('#technology img').forEach(o=>{o.classList.remove('anim-hide');}); 

      animateCSS($('#technology h2')[0],'fadeIn');
      $('#technology img').each((k,e)=>{e.classList.remove('animated')})      
      $('#technology img').each((k,e)=>{
        animateCSS(e,'fadeIn')
      })      
    }
  }
  if( typeof $('.wow').offset()  != "undefined") {

    if (isScrolledIntoView($('.wow'), $window) && scrollStopEvent.wow == false) {
      scrollStopEvent.wow = true;
      animateCSS($('.wow')[0],'fadeIn',function(){
        document.querySelectorAll('.wow').forEach(o=>{o.classList.remove('anim-hide');}); 
      });    

    }    
  }
  if( typeof $('.products').offset()  != "undefined") {

    if (isScrolledIntoView($('.products'), $window) && scrollStopEvent.products == false) {
      scrollStopEvent.products = true;
      animateCSS($('.products')[0],'fadeIn',function(){
        document.querySelectorAll('.products').forEach(o=>{o.classList.remove('anim-hide');}); 
      });    
    }    
  }
  if( typeof $('.cta').offset()  != "undefined") {

    if (isScrolledIntoView($(".cta"), $window) && scrollStopEvent.cta == false) {
      scrollStopEvent.cta = true;
    
      animateCSS($('.cta h2')[0],'fadeIn',function(){
        document.querySelectorAll('.cta h2').forEach(o=>{o.classList.remove('anim-hide');}); 
      });    
      animateCSS($('.cta .col-lg-4')[0],'fadeIn', () => {
        document.querySelectorAll('.cta .col-lg-4').forEach(o=>{o.classList.remove('anim-hide');}); 
      });    
      animateCSS($('.cta .col-lg-8')[0],'fadeIn', ()=> {
        document.querySelectorAll('.cta .col-lg-8').forEach(o=>{o.classList.remove('anim-hide');}); 
      });
    } 
  }
  
}); 