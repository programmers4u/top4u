
/** 
 * CAROUSEL
 * sceny: -35,-35sb,0,3,35sb,35
*/
/**
 * 
 * @param {string} scena 
 */
var setScene = (scena) => {
    let positions = scena.split(',');
    let returnObject = [
      //position
    ];
    
    for(let a=0; a < positions.length; a++){
      let position = {left: 0, opacity: 0, scale: {'a':true,'b':false}, blur: 0, skew: false};
      position.left = positions[a].replace(/[a-z]/gi,'');
      position.scale.a = (positions[a].search(/s/gi) > -1) ? true : false;
      position.scale.b = (positions[a].search(/ss/gi) > -1) ? true : false;
  //    position.scale.c = (positions[a].search(/s3/gi) > -1) ? true : false;
      position.blur = (positions[a].search(/b/gi) > -1) ? true : false;
      position.opacity = (positions[a].search(/o/gi) > -1) ? true : false;
      position.skew = (positions[a].search(/skw/gi) > -1) ? true : false;
      returnObject.push(position);
    }
    return returnObject;
  };
  
  let sc_ = '-35,-35sb,0,3,35sb,35';
  let sc = setScene(sc_);
  
  var setSlide = function(indx){
    if(carouselLoop >= 6) carouselLoop = 0;
    if(carouselLoop <= 0) carouselLoop = 6;
    //clearInterval(carouselInterval);
    if(indx>=6) {
      indx=0;
      carouselLoop = 0;
    }
  
      let left = [];
      let duration = 400;
      let timeout = 600;
      let scena = indx;
      let scale = [];
      let opacity = [];	
    let Obj = null;
  
    if(scena == 0){
      let position = '0,-65b,-35sb,10ssb,35sb,65b';
      Obj = setScene(position);
      }  
      if(scena == 1){
      let position = '-65b,-35sb,10ssb,35sb,65b,0';
      Obj = setScene(position);
      } 
      if(scena == 2){
      let position = '-35sb,10ssb,35sb,65b,0,-65b';
      Obj = setScene(position);
      }
      if(scena == 3){
      let position = '10ssb,35sb,65b,0,-65b,-35sb';
      Obj = setScene(position);
      }
      if(scena == 4){
      let position = '35sb,65b,0,-65b,-35sb,10ssb';
      Obj = setScene(position);
      }
      if(scena == 5){
      let position = '65b,0,-65b,-35sb,10ssb,35sb';
      Obj = setScene(position);
      }
   
      $('.wow2 .paginator i').each(function(){
          $(this).removeClass("fa fa-circle fa-circle-o");
          $(this).addClass("fa fa-circle");
    });
    let scenIndex = (scena==0) ? 0 : 6-scena;
      $($('.wow2 .paginator i')[scenIndex]).removeClass("fa fa-circle");
      $($('.wow2 .paginator i')[scenIndex]).addClass("fa fa-circle-o");
  
      for(let x=0;x<Obj.length;x++){
          $('.carousel-item'+x).animate({left:Obj[x].left+'%'},duration);	
          if(Obj[x].scale.a == true){
              setTimeout( function(x){
                  $('.carousel-item'+x).addClass('animate-scale-1');
              }, timeout, x );			
          } else {
              $('.carousel-item'+x).removeClass('animate-scale-1');	
          }
          if(Obj[x].scale.b == true){
              setTimeout( function(x){
                  $('.carousel-item'+x).addClass('animate-scale-2');
              }, timeout, x );			
          } else {
              $('.carousel-item'+x).removeClass('animate-scale-2');	
      }
          if(Obj[x].scale.c == true){
              setTimeout( function(x){
                  $('.carousel-item'+x).addClass('animate-scale-3');
              }, timeout, x );			
          } else {
              $('.carousel-item'+x).removeClass('animate-scale-3');	
          }
      
      if(Obj[x].blur == true){
              setTimeout( function(x){
                  $('.carousel-item'+x).addClass('animate-blur');
              }, timeout, x );			
          } else {
              $('.carousel-item'+x).removeClass('animate-blur');						
      }
                      
          if(Obj[x].opacity == true) {
              $('.carousel-item'+x).animate({opacity:0},20);	
          } else {
              $('.carousel-item'+x).animate({opacity:1},20);	
          }
  
      if(Obj[x].skew == true) {
        $('.carousel-item'+x).addClass('skew-1');
          } else {
              $('.carousel-item'+x).removeClass('skew-1');						
          }
                      
          if(Obj[x].left==0){
        //$('.carousel-item'+x).addClass('shadow');	
        //$('.carousel-item'+x).addClass('sepia');	
  
        $('.carousel-content'+x).animate({left:'0%'},1);	
              $('.carousel-content'+x).css('display','inline-block');	
              $('.carousel-content'+x).animate({opacity:1},1);	
              if (document.documentElement.clientWidth >= 1366 && 1900 > document.documentElement.clientWidth) {
                  $('.carousel-content'+x).animate({left: ($('.carousel-content'+x).width()-80)+'px'},1400);	
              } else if(document.documentElement.clientWidth > 1900) {
                  $('.carousel-content'+x).animate({left: ($('.carousel-content'+x).width()+260)+'px'},1400);	
              } else {
                  $('.carousel-content'+x).animate({left: ($('.carousel-content'+x).width()-80)+'px'},1400);	
        }
          }	
          if(Obj[x].left==65){
        //$('.carousel-item'+x).removeClass('shadow');	
        //$('.carousel-item'+x).removeClass('sepia');	
  
              $('.carousel-content'+x).css('display','inline-block');	
              $('.carousel-content'+x).animate({left:'100%'},400).promise().done(function(x){
                  $('.carousel-content'+x).animate({opacity:0},1).promise().done(function(x){
            $('.carousel-content'+x).animate({left:'0%'},1);	
          }(x));	
              }(x));	
          }	
          if(Obj[x].left==-35){
              //$('.carousel-content'+x).animate({opacity:0},1);	
              //$('.carousel-content'+x).animate({left:'0%'},1);	
              //$('.carousel-content'+x).css('display','none');	
          }	
      }
  }
  
  var carouselLoop = 0;
  var carouselInterval = null;

      //setSlide(carouselLoop); 

   // $('.carousel').carousel()
