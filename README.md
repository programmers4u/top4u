# Programmers theme for OctoberCMS

Programmers is a Bootstrap app landing page theme to help you beautifully showcase your web app or anything else!

Template created by [Start Bootstrap](https://startbootstrap.com/template-overviews/new-age/).

## Features

- Fully responsive
- Complete content management
- You can enable/disable all sections
- Separated feature list management
- Semantic markup with nav, sections, and asides
- Font Awesome and Simple Line Icons included

## Contributing

**Feel free to send pullrequest!** Please send pull request to master branch.

## Future plans

- [ ] add favicon
- [ ] checkbox and styles for non-fixed main menu
- [ ] prepare blank page for FAQs or other stuff

## Copyright and License

OctoberCMS theme: Copyright 2019 Marcin Kania.